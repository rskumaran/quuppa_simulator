package com.entappia.ei40qpesimulator;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.ResourceBundle;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.Scheduled;

import com.entappia.ei40qpesimulator.controllers.QPEController;
import com.entappia.ei40qpesimulator.utils.CampusDetails;
import com.entappia.ei40qpesimulator.utils.ZoneDetails;
import com.entappia.ei40qpesimulator.utils.ZoneDetails.CoordinateList;
import com.entappia.ei40qpesimulator.utils.ZoneDetails.Zone;
import com.google.gson.Gson;
import com.snatik.polygon.Point;
import com.snatik.polygon.Polygon;



@SpringBootApplication
public class ei4oQpeSimulatorApplication {


	private CampusDetails campusDetailsObj = new CampusDetails();
	private ZoneDetails zoneDetailsObj = new ZoneDetails();
	//private QPEController qpeController = new QPEController();

	public static void main(String[] args) {
		SpringApplication.run(ei4oQpeSimulatorApplication.class, args);
	}
/*	@PostConstruct
	public void onApplicationStart() {
		String dir = getDirectory();
		File f = new File(dir);

		if (!f.isDirectory()) {
			System.out.println("The path specified is not directory");
			return;
		}

		File f1 = new File(dir + "/" + "ZoneDetails" + ".json");
		if (!f1.isFile()) {
			System.out.println(" " + "file not found");
		}


		String zoneDetails = getDataFromFile(f1.getAbsolutePath());
		Gson gson = new Gson();
		zoneDetailsObj = gson.fromJson(zoneDetails, ZoneDetails.class);
		System.out.println(zoneDetailsObj.getVersion());

		f1 = new File(dir + "/" + "ZoneDetails" + ".json");
		if (!f1.isFile()) {
			System.out.println(" " + "file not found");
		}


		String campusDetails = getDataFromFile(f1.getAbsolutePath());
		campusDetailsObj = gson.fromJson(campusDetails, CampusDetails.class);

		//generateTagPositions(zoneDetailsObj,campusDetailsObj);
		//qpeController.set(zoneDetailsObj);
	}

	//@Scheduled(cron = "0/1 0 * * * *")
	

	private void generateTagPositions(ZoneDetails lzoneDetailsObj, CampusDetails lcampusDetailsObj) {
		
		System.out.println("Run Time:-" + new Date());
		double minx = 0.00;
		double maxx = 98.00;
		double miny = 0.00;
		double maxy = 66.00;
		double randomx = 0.00;
		double randomy = 0.00;

		List<Zone> zoneList= lzoneDetailsObj.getZone();
		List<HashMap<String, String>> coordinateListNew = new ArrayList<>();
		for(int j = 0; j < 100/*tags count; j++) {

			
			int zoneCount = 0;
			
			randomx = minx + Math.random() * (maxx - minx);
			randomy = miny + Math.random() * (maxy - miny);
			
			for(int i = 0; i < zoneList.size(); i++ ) {
				Zone zone = zoneList.get(i);
				List<CoordinateList> coordinateList= zone.getCoordinateList();
				coordinateListNew.clear();
				for(int k = 0; k < coordinateList.size();k++ ) {
					HashMap<String, String> coordinatesMap = new HashMap<String, String>();
					coordinatesMap.put("x",coordinateList.get(k).getX());
					coordinatesMap.put("y",coordinateList.get(k).getY());
					coordinateListNew.add(coordinatesMap);
				}

				
				
				if(isInsidePolygon(coordinateListNew, randomx, randomy)) {
					System.out.println(" Tag id : " + j + "Zone Name : " + zone.getName() 
					+ "Zone id : " + zone.getId() + "xpos: " + randomx + "ypos :" + randomy);
					zoneCount++;
					
			}
				
			}
			if(zoneCount == 0 ) {
				System.out.println("Zone not found for Tagid : " + j + "xpos: " + randomx + "ypos :" + randomy);
			}
			if(zoneCount > 1 ) {
				System.out.println("More than one Zone assigned for Tag id : " + j + "xpos: " + randomx + "ypos :" + randomy);
			}


		}
	}
	private boolean isInsidePolygon(List<HashMap<String, String>> coordinateList, double x, double y) {
		Polygon.Builder polygonBuilder = Polygon.Builder();

		for (int j = 0; j < coordinateList.size(); j++) {
			HashMap<String, String> coordinate = coordinateList.get(j);

			double x1 = Double.valueOf(coordinate.get("x"));
			double y1 = Double.valueOf(coordinate.get("y"));

			polygonBuilder.addVertex(new Point(x1, y1));

		}

		polygonBuilder.close();

		Polygon polygon = polygonBuilder.build();
		Point point = new Point(x, y);
		boolean contains = polygon.contains(point);
		if (contains) {
			return true;
		} else
			return false;
	}

	public String getDirectory() {
		ResourceBundle bundle = ResourceBundle.getBundle("application");
		String os = System.getProperty("os.name").toLowerCase();
		String dir = "";
		if (os.contains("windows")) {
			dir = bundle.getString("filepath_windows");
		} else if (os.contains("mac os")) {
			dir = bundle.getString("filepath_ios");
		} else
			dir = bundle.getString("filepath_linux");

		return dir;
	}
	public static String getDataFromFile(String filePath) {

		InputStream resourcee;
		try {
			resourcee = new FileInputStream(new File(filePath));

			Reader initialReader = new InputStreamReader(resourcee);
			char[] arr = new char[8 * 1024];
			StringBuilder buffer = new StringBuilder();
			int numCharsRead;
			while ((numCharsRead = initialReader.read(arr, 0, arr.length)) != -1) {
				buffer.append(arr, 0, numCharsRead);
			}
			initialReader.close();
			String targetString = buffer.toString();
			return targetString;
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}*/

}
