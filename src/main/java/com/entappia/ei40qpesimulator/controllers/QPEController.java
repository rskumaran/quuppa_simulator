package com.entappia.ei40qpesimulator.controllers;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.concurrent.ExecutionException;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.entappia.ei40qpesimulator.utils.BaseController;
import com.entappia.ei40qpesimulator.utils.CampusDetails;
import com.entappia.ei40qpesimulator.utils.ProjectInfo;
import com.entappia.ei40qpesimulator.utils.TagData;
import com.entappia.ei40qpesimulator.utils.TagDetails;
import com.entappia.ei40qpesimulator.utils.Utils;
import com.entappia.ei40qpesimulator.utils.ZoneDetails;
import com.entappia.ei40qpesimulator.utils.TagDetails.Tags;
import com.entappia.ei40qpesimulator.utils.ZoneDetails.CoordinateList;
import com.entappia.ei40qpesimulator.utils.ZoneDetails.Zone;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.snatik.polygon.Point;
import com.snatik.polygon.Polygon;
import com.google.gson.*;

@RestController
public class QPEController extends BaseController {

	/*@PostConstruct
	public void onApplicationStart() {
		String dir = getDirectory();
		File f = new File(dir);

		if (!f.isDirectory()) {
			System.out.println("The path specified is not directory");
			return;
		}

		File f1 = new File(dir + "/" + "ZoneDetails" + ".json");
		if (!f1.isFile()) {
			System.out.println(" " + "file not found");
		}


		String zoneDetails = getDataFromFile(f1.getAbsolutePath());
		Gson gson = new Gson();
		zoneDetailsObj = gson.fromJson(zoneDetails, ZoneDetails.class);
		System.out.println(zoneDetailsObj.getVersion());


	}*/


	/*public QPEController(CampusDetails campusDetailsObj, ZoneDetails zoneDetailsObj) {
		this.campusDetailsObj = campusDetailsObj;
		this.zoneDetailsObj = zoneDetailsObj;
	}*/

	/*@RequestMapping(path = { "getTagData" }, method = RequestMethod.GET)
	public ResponseEntity<?> getTagData(HttpServletRequest request, HttpSession httpSession,
			@RequestBody HashMap<String, Object> requestmap) throws InterruptedException, ExecutionException {
		HashMap<String, Object> returnmap = new HashMap<>();

		int tagcount = 100;
		if (requestmap.containsKey("tagcount"))
		{
			if(!Utils.isEmptyString(requestmap.getOrDefault("tagcount", "").toString())) {
				tagcount = Integer.parseInt(requestmap.getOrDefault("tagcount", "").toString());
			}

		}
		return new ResponseEntity<>(returnmap, HttpStatus.OK);
	}*/

	//@RequestMapping(path = { "getTagData", "getTagData/{tagcount}", "getTagData/{zonecount}" }, method = RequestMethod.GET)
	/*@RequestMapping(value = "/getTagData", params = {"tagcount", "zonecount"}, method = RequestMethod.GET)
	public ResponseEntity<?> getEmployee(HttpServletRequest request, HttpSession httpSession,
			@PathVariable Optional<Integer> tagcount, @PathVariable Optional<Integer> zonecount)
			throws InterruptedException, ExecutionException {
		HashMap<String, Object> returnmap = new HashMap<>();

		return new ResponseEntity<>(returnmap, HttpStatus.OK);
	}*/

	@RequestMapping(value = "/getProjectInfo", method = RequestMethod.GET)
	public ResponseEntity<?> getProjectInfo(HttpServletRequest request, HttpSession httpSession )
			throws InterruptedException, ExecutionException {

		HashMap<String, Object> returnmap = new HashMap<>();
		String dir = getDirectory();
		File f = new File(dir);

		if (!f.isDirectory()) {
			returnmap.put("code", 7);
			returnmap.put("status", "Object not found");
			returnmap.put("message", dir + " Directory not found");
			returnmap.put("version", 1);
			returnmap.put("command", "getProjectInfo");
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

		File f1 = new File(dir + "/" + "ProjectInfo" + ".json");
		if (!f1.isFile()) {
			System.out.println(" " + "file not found");
			returnmap.put("code", 7);
			returnmap.put("status", "Object not found");
			returnmap.put("message", dir + "/" + "ProjectInfo" + ".json not found");
			returnmap.put("version", 1);
			returnmap.put("command", "getProjectInfo");
			return new ResponseEntity<>(returnmap, HttpStatus.OK);


		}

		String projectInfo = getDataFromFile(f1.getAbsolutePath());

		return new ResponseEntity<>(projectInfo, HttpStatus.OK);
	}

	@RequestMapping(value = "/getLocatorInfo", method = RequestMethod.GET)
	public ResponseEntity<?> getLocatorInfo(HttpServletRequest request, HttpSession httpSession)
			throws InterruptedException, ExecutionException {

		HashMap<String, Object> returnmap = new HashMap<>();
		String dir = getDirectory();
		File f = new File(dir);

		if (!f.isDirectory()) {
			returnmap.put("code", 7);
			returnmap.put("status", "Object not found");
			returnmap.put("message", dir + " Directory not found");
			returnmap.put("version", 1);
			returnmap.put("command", "getLocatorInfo");
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

		File f1 = new File(dir + "/" + "LocatorInfo" + ".json");
		if (!f1.isFile()) {
			System.out.println(" " + "file not found");
			returnmap.put("code", 7);
			returnmap.put("status", "Object not found");
			returnmap.put("message", dir + "/" + "LocatorInfo" + ".json not found");
			returnmap.put("version", 1);
			returnmap.put("command", "getLocatorInfo");
			return new ResponseEntity<>(returnmap, HttpStatus.OK);


		}

		String locatorInfo = getDataFromFile(f1.getAbsolutePath());

		return new ResponseEntity<>(locatorInfo, HttpStatus.OK);
	}
	@RequestMapping(value = "/getPEInfo", method = RequestMethod.GET)
	public ResponseEntity<?> getPEInfo(HttpServletRequest request, HttpSession httpSession)
			throws InterruptedException, ExecutionException {

		HashMap<String, Object> returnmap = new HashMap<>();
		String dir = getDirectory();
		File f = new File(dir);

		if (!f.isDirectory()) {
			returnmap.put("code", 7);
			returnmap.put("status", "Object not found");
			returnmap.put("message", dir + " Directory not found");
			returnmap.put("version", 1);
			returnmap.put("command", "getPEInfo");
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

		File f1 = new File(dir + "/" + "qpeInfo" + ".json");
		if (!f1.isFile()) {
			System.out.println(" " + "file not found");
			returnmap.put("code", 7);
			returnmap.put("status", "Object not found");
			returnmap.put("message", dir + "/" + "qpeInfo" + ".json not found");
			returnmap.put("version", 1);
			returnmap.put("command", "getPEInfo");
			return new ResponseEntity<>(returnmap, HttpStatus.OK);


		}

		String qpeInfo = getDataFromFile(f1.getAbsolutePath());

		return new ResponseEntity<>(qpeInfo, HttpStatus.OK);
	}

	@RequestMapping(value = "/getTagData", method = RequestMethod.GET)
	public ResponseEntity<?> getTagData(HttpServletRequest request, HttpSession httpSession,
			@RequestParam(value = "tagcount", required = false, defaultValue = "100" ) long tagcount, 
			@RequestParam(value = "zonecount", required = false, defaultValue = "50") long zonecount )
					throws InterruptedException, ExecutionException {
		HashMap<String, Object> returnmap = new HashMap<>();
		String dir = getDirectory();
		File f = new File(dir);
		
		ZoneDetails zoneDetailsObj = new ZoneDetails();

		if (!f.isDirectory()) {
			//System.out.println("The path specified is not directory");
			returnmap.put("code", 7);
			returnmap.put("status", "Object not found");
			returnmap.put("message", dir + " Directory not found");
			returnmap.put("version", 1);
			returnmap.put("command", "getTagData");
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

		File f1 = new File(dir + "/" + "ZoneDetails" + ".json");
		if (!f1.isFile()) {
			returnmap.put("code", 7);
			returnmap.put("status", "Object not found");
			returnmap.put("message", dir + "/" + "ProjectInfo" + ".json not found");
			returnmap.put("version", 1);
			returnmap.put("command", "getTagData");
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}


		String zoneDetails = getDataFromFile(f1.getAbsolutePath());
		Gson gson = new Gson();
		zoneDetailsObj = gson.fromJson(zoneDetails, ZoneDetails.class);
		//System.out.println(zoneDetailsObj.getVersion());
		
		return generateTagPositions(zoneDetailsObj);
		//return new ResponseEntity<>(returnmap, HttpStatus.OK);
	}

	/*@RequestMapping(value = "/getTagData", params = {"tagcount", "zonecount"}, method = RequestMethod.GET)
	public ResponseEntity<?> getTagData(HttpServletRequest request, HttpSession httpSession,
			@RequestParam("tagcount") long tagcount, @RequestParam("zonecount") long zonecount )
			throws InterruptedException, ExecutionException {
		HashMap<String, Object> returnmap = new HashMap<>();
		System.out.println(zonecount);
		System.out.println(tagcount);
		return new ResponseEntity<>(returnmap, HttpStatus.OK);
	}*/

	private ResponseEntity<?> generateTagPositions(ZoneDetails lzoneDetailsObj) {

		//Get origin details from project info file
		//System.out.println("QPE Simulator generateTagPositions Run Time:-" + Utils.getFormatedDate2(new Date()));

		HashMap<String, Object> returnmap = new HashMap<>();
		TagData tagData = new TagData();

		String dir = getDirectory();
		File f = new File(dir);

		if (!f.isDirectory()) {
			returnmap.put("code", 7);
			returnmap.put("status", "Object not found");
			returnmap.put("message", dir + " Directory not found");
			returnmap.put("version", 1);
			returnmap.put("command", "getTagData");
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

		File f1 = new File(dir + "/" + "ProjectInfo" + ".json");
		if (!f1.isFile()) {
			System.out.println(" " + "file not found");
			returnmap.put("code", 7);
			returnmap.put("status", "Object not found");
			returnmap.put("message", dir + "/" + "ProjectInfo" + ".json not found");
			returnmap.put("version", 1);
			returnmap.put("command", "getTagData");
			return new ResponseEntity<>(returnmap, HttpStatus.OK);


		}

		String projectInfo = getDataFromFile(f1.getAbsolutePath());
		ProjectInfo projectInfoObj = new ProjectInfo();
		Gson gson = new Gson();
		projectInfoObj = gson.fromJson(projectInfo, ProjectInfo.class);
		double minx = projectInfoObj.getminx();
		double maxx = projectInfoObj.getmaxx();
		double miny = projectInfoObj.getminy();
		double maxy = projectInfoObj.getmaxy();

		//System.out.println("Run Time:-" + new Date());
		/*double minx = 0.00;
		double maxx = 98.00;
		double miny = 0.00;
		double maxy = 66.00;*/
		//Get Tag details from file or generate and store it in file
		f1 = new File(dir + "/" + "Tags" + ".json");
		if (!f1.isFile()) {
			System.out.println(" " + "file not found");
			returnmap.put("code", 7);
			returnmap.put("status", "Object not found");
			returnmap.put("message", dir + "/" + "Tags" + ".json not found");
			returnmap.put("version", 1);
			returnmap.put("command", "getTagData");
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

		String tagDetails = getDataFromFile(f1.getAbsolutePath());
		TagDetails tagDetailsObj = new TagDetails();
		//Gson gson = new Gson();
		tagDetailsObj = gson.fromJson(tagDetails, TagDetails.class);

		List<Tags> tagList = tagDetailsObj.getTags();
		
		if (tagList == null || tagList.size() <= 0) {
			System.out.println(" " + "taglist not found");
			returnmap.put("code", 7);
			returnmap.put("status", "Object not found");
			returnmap.put("message", "taglist not found");
			returnmap.put("version", 1);
			returnmap.put("command", "getTagData");
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}

		Double randomx = 0.00;
		Double randomy = 0.00;

		
		
		//List<Zone> zoneList= lzoneDetailsObj.getZone();
		List<Zone> zoneList= lzoneDetailsObj.getTrackingZones();
		if (zoneList == null || zoneList.size() <= 0) {
			System.out.println(" " + "zonelist not found");
			returnmap.put("code", 7);
			returnmap.put("status", "Object not found");
			returnmap.put("message", "zonelist not found");
			returnmap.put("version", 1);
			returnmap.put("command", "getTagData");
			return new ResponseEntity<>(returnmap, HttpStatus.OK);
		}
		
		List<HashMap<String, String>> coordinateListNew = new ArrayList<>();
		
		for(int j = 0; j < tagList.size()/*tags count*/; j++) {

			Tags tags = tagList.get(j);
			int zoneCount = 0;
			int noOfTries = 25;//Magic
			int noTried = 0;
			
			do {
				noTried++;
				randomx = minx + Math.random() * (maxx - minx);
				randomy = miny + Math.random() * (maxy - miny);
				
				double originX = projectInfoObj.getOriginX() * projectInfoObj.getMetersPerPixelX();
				double originY = projectInfoObj.getOriginY() * projectInfoObj.getMetersPerPixelY();
				
				Double randomxNew = Math.abs(originX - randomx);
				Double randomyNew = Math.abs(originY - randomy);
				
				randomxNew = randomx;
				randomyNew = randomy;
				
			for(int i = 0; i < zoneList.size(); i++ ) {
				Zone zone = zoneList.get(i);
				List<CoordinateList> coordinateList= zone.getCoordinateList();
				coordinateListNew.clear();
				for(int k = 0; k < coordinateList.size();k++ ) {
					HashMap<String, String> coordinatesMap = new HashMap<String, String>();
					coordinatesMap.put("x",coordinateList.get(k).getX());
					coordinatesMap.put("y",coordinateList.get(k).getY());
					coordinateListNew.add(coordinatesMap);
				}

				if(isInsidePolygon(coordinateListNew, randomxNew, randomyNew)) {
					for(int yyy = 0; yyy < coordinateListNew.size(); yyy++) {
						HashMap<String, String> coordinate = coordinateListNew.get(yyy);

						double x1 = Double.valueOf(coordinate.get("x"));
						double y1 = Double.valueOf(coordinate.get("y"));
						
						//System.out.println(" Tag id : " + tags.getMacId() + "Zone Name : " + zone.getName() 
						//+ "Zone id : " + zone.getId() + "xpos: " + x1 + "ypos :" + y1 + "randomx : " + randomx + "randomy : " + randomy);
					}
					//System.out.println(" Tag id : " + tags.getMacId() + "Zone Name : " + zone.getName() 
					//+ "Zone id : " + zone.getId() + "xpos: " + randomx + "ypos :" + randomy);
					zoneCount++;
					break; // come out of for  i loop

				}
				
			}
				/*if(noTried == noOfTries) {
					break;// come out of do while loop
				}*/
			}while(zoneCount == 0 );

			if(zoneCount == 0 ) {
				System.out.println("Zone not found for Tagid : " + tags.getTagId() + "xpos: " + randomx + "ypos :" + randomy + "noTried : " + noTried );
			}
			DecimalFormat df = new DecimalFormat("#.##");
			if(zoneCount > 1 ) {
				System.out.println("More than one Zone assigned for Tag id : " + tags.getTagId() + "xpos: " + randomx + "ypos :" + randomy);
				//System.out.println("More than one Zone assigned for Tag id : " + tags.getTagId() + "xpos: " + round(randomx, 2) + "ypos :" + round(randomy, 2));
			}

			
			tagData.setCode("0");
			tagData.setStatus("Ok");
			tagData.setCommand("getTagData");
			tagData.setMessage("Tag data");
			tagData.setResponseTS(System.currentTimeMillis());
			tagData.setVersion("1.0");
			tagData.setFormatId("defaultLocationAndInfo");
			tagData.setFormatName("defaultLocationAndInfo");
			tagData.setTags(tags.getTagId(), tags.getMacId(), System.currentTimeMillis(), randomx, randomy, 1.20/*height do this somewhere else*/);

		}
		
		
		/*Gson gsonNew = new GsonBuilder().setPrettyPrinting().create();
		String JsonString = gsonNew.toJson(tagData);
		
		JsonParser jp = new JsonParser();
		JsonElement je = jp.parse(JsonString);
		String JsonStringnew = gsonNew.toJson(je);*/
		
		
		
		
		
		return new ResponseEntity<>(tagData, HttpStatus.OK);
	}
	private boolean isInsidePolygon(List<HashMap<String, String>> coordinateList, double x, double y) {
		Polygon.Builder polygonBuilder = Polygon.Builder();

		for (int j = 0; j < coordinateList.size(); j++) {
			HashMap<String, String> coordinate = coordinateList.get(j);

			double x1 = Double.valueOf(coordinate.get("x"));
			double y1 = Double.valueOf(coordinate.get("y"));

			polygonBuilder.addVertex(new Point(x1, y1));

		}

		polygonBuilder.close();

		Polygon polygon = polygonBuilder.build();
		Point point = new Point(x, y);
		boolean contains = polygon.contains(point);
		if (contains) {
			return true;
		} else
			return false;
	}

	public String getDirectory() {
		ResourceBundle bundle = ResourceBundle.getBundle("application");
		String os = System.getProperty("os.name").toLowerCase();
		String dir = "";
		if (os.contains("windows")) {
			dir = bundle.getString("filepath_windows");
		} else if (os.contains("mac os")) {
			dir = bundle.getString("filepath_ios");
		} else
			dir = bundle.getString("filepath_linux");

		return dir;
	}
	public static String getDataFromFile(String filePath) {

		InputStream resourcee;
		try {
			resourcee = new FileInputStream(new File(filePath));

			Reader initialReader = new InputStreamReader(resourcee);
			char[] arr = new char[8 * 1024];
			StringBuilder buffer = new StringBuilder();
			int numCharsRead;
			while ((numCharsRead = initialReader.read(arr, 0, arr.length)) != -1) {
				buffer.append(arr, 0, numCharsRead);
			}
			initialReader.close();
			String targetString = buffer.toString();
			return targetString;
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

	/*public void set(ZoneDetails zoneDetailsObj) {
		this.zoneDetailsObj = zoneDetailsObj;

	}*/
}


