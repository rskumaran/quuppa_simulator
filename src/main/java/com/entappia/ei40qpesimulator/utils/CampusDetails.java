package com.entappia.ei40qpesimulator.utils;

public class CampusDetails {

	  private int campusId; 
	  private String colorCode; 
	  private String createdDate; 
	  private int height; 
	  private String isActive; 
	  private int metersPerPixelx; 
	  private int metersPerPixely; 
	  private String modifiedDate; 
	  private String name; 
	  private int originx; 
	  private int originy; 
	  private int width; 

	  public int getCampusId(){
	  	return campusId; 
	  }
	  public void setCampusId(int input){
	  	 this.campusId = input;
	  }
	  public String getColorCode(){
	  	return colorCode; 
	  }
	  public void setColorCode(String input){
	  	 this.colorCode = input;
	  }
	  public String getCreatedDate(){
	  	return createdDate; 
	  }
	  public void setCreatedDate(String input){
	  	 this.createdDate = input;
	  }
	  public int getHeight(){
	  	return height; 
	  }
	  public void setHeight(int input){
	  	 this.height = input;
	  }
	  public String getIsActive(){
	  	return isActive; 
	  }
	  public void setIsActive(String input){
	  	 this.isActive = input;
	  }
	  public int getMetersPerPixelx(){
	  	return metersPerPixelx; 
	  }
	  public void setMetersPerPixelx(int input){
	  	 this.metersPerPixelx = input;
	  }
	  public int getMetersPerPixely(){
	  	return metersPerPixely; 
	  }
	  public void setMetersPerPixely(int input){
	  	 this.metersPerPixely = input;
	  }
	  public String getModifiedDate(){
	  	return modifiedDate; 
	  }
	  public void setModifiedDate(String input){
	  	 this.modifiedDate = input;
	  }
	  public String getName(){
	  	return name; 
	  }
	  public void setName(String input){
	  	 this.name = input;
	  }
	  public int getOriginx(){
	  	return originx; 
	  }
	  public void setOriginx(int input){
	  	 this.originx = input;
	  }
	  public int getOriginy(){
	  	return originy; 
	  }
	  public void setOriginy(int input){
	  	 this.originy = input;
	  }
	  public int getWidth(){
	  	return width; 
	  }
	  public void setWidth(int input){
	  	 this.width = input;
	  }
}
  

