package com.entappia.ei40qpesimulator.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Utils {

	  
	public static boolean isEmptyString(String value) {
		return value == null || value.trim().isEmpty() || value.equals("null");
	}
	
	public static String getFormatedDate2(Date date) {
		SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MMM-dd HH:mm:ss.SSS");
		return dateTimeFormat.format(date);

	}
	 
	
		
}
