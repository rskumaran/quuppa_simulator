package com.entappia.ei40qpesimulator.utils;

import java.util.ArrayList;
import java.util.List;

import com.entappia.ei40qpesimulator.utils.ZoneDetails.Zone;


public class ZoneDetails{
	
	public class Classification{
		  private int id; 
		  private String className; 
		  private String color; 

		  public int getId(){
		  	return id; 
		  }
		  public void setId(int input){
		  	 this.id = input;
		  }
		  public String getClassName(){
		  	return className; 
		  }
		  public void setClassName(String input){
		  	 this.className = input;
		  }
		  public String getColor(){
		  	return color; 
		  }
		  public void setColor(String input){
		  	 this.color = input;
		  }
	}
	public class CoordinateList{
		  private String x; 
		  private String y; 

		  public String getX(){
		  	return x; 
		  }
		  public void setX(String input){
		  	 this.x = input;
		  }
		  public String getY(){
		  	return y; 
		  }
		  public void setY(String input){
		  	 this.y = input;
		  }
	}
	/*public class GridList{

	}*/
	public class Zone{
		  private int id; 
		  private int campusId; 
		  private String name; 
		  private String type; 
		  private String accessControl; 
		  private String zoneType; 
		  private int zoneClassification; 
		  private String area; 
		  private String geometryType; 
		  private int capacity; 
		  private String description; 
		  private List<CoordinateList> coordinateList; 
		  private List<String> gridList; 

		  public int getId(){
		  	return id; 
		  }
		  public void setId(int input){
		  	 this.id = input;
		  }
		  public int getCampusId(){
		  	return campusId; 
		  }
		  public void setCampusId(int input){
		  	 this.campusId = input;
		  }
		  public String getName(){
		  	return name; 
		  }
		  public void setName(String input){
		  	 this.name = input;
		  }
		  public String getType(){
		  	return type; 
		  }
		  public void setType(String input){
		  	 this.type = input;
		  }
		  public String getAccessControl(){
		  	return accessControl; 
		  }
		  public void setAccessControl(String input){
		  	 this.accessControl = input;
		  }
		  public String getZoneType(){
		  	return zoneType; 
		  }
		  public void setZoneType(String input){
		  	 this.zoneType = input;
		  }
		  public int getZoneClassification(){
		  	return zoneClassification; 
		  }
		  public void setZoneClassification(int input){
		  	 this.zoneClassification = input;
		  }
		  public String getArea(){
		  	return area; 
		  }
		  public void setArea(String input){
		  	 this.area = input;
		  }
		  public String getGeometryType(){
		  	return geometryType; 
		  }
		  public void setGeometryType(String input){
		  	 this.geometryType = input;
		  }
		  public int getCapacity(){
		  	return capacity; 
		  }
		  public void setCapacity(int input){
		  	 this.capacity = input;
		  }
		  public String getDescription(){
		  	return description; 
		  }
		  public void setDescription(String input){
		  	 this.description = input;
		  }
		  public List<CoordinateList> getCoordinateList(){
		  	return coordinateList; 
		  }
		  public void setCoordinateList(List<CoordinateList> input){
		  	 this.coordinateList = input;
		  }
		  public List<String> getGridList(){
		  	return gridList; 
		  }
		  public void setGridList(List<String> input){
		  	 this.gridList = input;
		  }
	}
	  private String version; 
	  private String type; 
	  private List<Classification> classification; 
	  private List<Zone> zone; 

	  public String getVersion(){
	  	return version; 
	  }
	  public void setVersion(String input){
	  	 this.version = input;
	  }
	  public String getType(){
	  	return type; 
	  }
	  public void setType(String input){
	  	 this.type = input;
	  }
	  public List<Classification> getClassification(){
	  	return classification; 
	  }
	  public void setClassification(List<Classification> input){
	  	 this.classification = input;
	  }
	  public List<Zone> getZone(){
	  	return zone; 
	  }
	  public void setZone(List<Zone> input){
	  	 this.zone = input;
	  }
	public List<Zone> getTrackingZones() {
		List<Zone> trackingZones = new ArrayList<Zone>();
		for(int i = 0; i < zone.size(); i++ ) {
			Zone zoneItem = zone.get(i);
			
			if(zoneItem.getZoneType().compareToIgnoreCase("Trackingzone") == 0 ) {
				trackingZones.add(zoneItem);
			}
		}
		return trackingZones;
	}
}
  
