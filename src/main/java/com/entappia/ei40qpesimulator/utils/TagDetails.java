package com.entappia.ei40qpesimulator.utils;

import java.util.List;


public class TagDetails{
	
	public class Tags{
		  private String macId; 
		  private String tagId; 

		  public String getMacId(){
		  	return macId; 
		  }
		  public void setMacId(String input){
		  	 this.macId = input;
		  }
		  public String getTagId(){
		  	return tagId; 
		  }
		  public void setTagId(String input){
		  	 this.tagId = input;
		  }
	}
	  private List<Tags> tags; 

	  public List<Tags> getTags(){
	  	return tags; 
	  }
	  public void setTags(List<Tags> input){
	  	 this.tags = input;
	  }
}
  
