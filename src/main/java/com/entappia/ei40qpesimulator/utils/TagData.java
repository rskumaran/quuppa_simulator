package com.entappia.ei40qpesimulator.utils;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;



public class TagData{
	
		public class Tags{
		  private String tagId; 
		  private String tagName; 
		  private int lastPacketTS; 
		  private String color; 
		  private String tagGroupName; 
		  private String locationType; 
		  private String locationMovementStatus; 
		  private int locationRadius; 
		  private List<Double> location; 
		  private long locationTS; 
		  private String locationCoordSysId; 
		  private String locationCoordSysName;
		  private List<String>locationZoneIds;
		  private List<String>locationZoneNames;
		  private String button1State; 
		  private int button1StateTS; 
		  private int button1LastPressTS; 
		  private String batteryAlarm; 
		  private int batteryAlarmTS; 
		  private int rssi; 
		  private int rssiLocatorCount; 
		  
		  
		public List<Double> getLocation() {
			return location;
		}
		public void setLocation(List<Double> location) {
			this.location = location;
		}
		public List<String> getLocationZoneIds() {
			return locationZoneIds;
		}
		public void setLocationZoneIds(List<String> locationZoneIds) {
			this.locationZoneIds = locationZoneIds;
		}
		public List<String> getLocationZoneNames() {
			return locationZoneNames;
		}
		public void setLocationZoneNames(List<String> locationZoneNames) {
			this.locationZoneNames = locationZoneNames;
		}
		

		  public String getTagId(){
		  	return tagId; 
		  }
		  public void setTagId(String input){
		  	 this.tagId = input;
		  }
		  public String getTagName(){
		  	return tagName; 
		  }
		  public void setTagName(String input){
		  	 this.tagName = input;
		  }
		  public int getLastPacketTS(){
		  	return lastPacketTS; 
		  }
		  public void setLastPacketTS(int input){
		  	 this.lastPacketTS = input;
		  }
		  public String getColor(){
		  	return color; 
		  }
		  public void setColor(String input){
		  	 this.color = input;
		  }
		  public String getTagGroupName(){
		  	return tagGroupName; 
		  }
		  public void setTagGroupName(String input){
		  	 this.tagGroupName = input;
		  }
		  public String getLocationType(){
		  	return locationType; 
		  }
		  public void setLocationType(String input){
		  	 this.locationType = input;
		  }
		  public String getLocationMovementStatus(){
		  	return locationMovementStatus; 
		  }
		  public void setLocationMovementStatus(String input){
		  	 this.locationMovementStatus = input;
		  }
		  public int getLocationRadius(){
		  	return locationRadius; 
		  }
		  public void setLocationRadius(int input){
		  	 this.locationRadius = input;
		  }
		  public long getLocationTS(){
		  	return locationTS; 
		  }
		  public void setLocationTS(long input){
		  	 this.locationTS = input;
		  }
		  public String getLocationCoordSysId(){
		  	return locationCoordSysId; 
		  }
		  public void setLocationCoordSysId(String input){
		  	 this.locationCoordSysId = input;
		  }
		  public String getLocationCoordSysName(){
		  	return locationCoordSysName; 
		  }
		  public void setLocationCoordSysName(String input){
		  	 this.locationCoordSysName = input;
		  }
		  public String getButton1State(){
		  	return button1State; 
		  }
		  public void setButton1State(String input){
		  	 this.button1State = input;
		  }
		  public int getButton1StateTS(){
		  	return button1StateTS; 
		  }
		  public void setButton1StateTS(int input){
		  	 this.button1StateTS = input;
		  }
		  public int getButton1LastPressTS(){
		  	return button1LastPressTS; 
		  }
		  public void setButton1LastPressTS(int input){
		  	 this.button1LastPressTS = input;
		  }
		  public String getBatteryAlarm(){
		  	return batteryAlarm; 
		  }
		  public void setBatteryAlarm(String input){
		  	 this.batteryAlarm = input;
		  }
		  public int getBatteryAlarmTS(){
		  	return batteryAlarmTS; 
		  }
		  public void setBatteryAlarmTS(int input){
		  	 this.batteryAlarmTS = input;
		  }
		  public int getRssi(){
		  	return rssi; 
		  }
		  public void setRssi(int input){
		  	 this.rssi = input;
		  }
		  public int getRssiLocatorCount(){
		  	return rssiLocatorCount; 
		  }
		  public void setRssiLocatorCount(int input){
		  	 this.rssiLocatorCount = input;
		  }
	}
	  private String code; 
	  private String status; 
	  private String command; 
	  private String message; 
	  private long responseTS; 
	  private String version; 
	  private String formatId; 
	  private String formatName; 
	  private List<Tags> tags = new ArrayList<Tags>(); 

	  public String getCode(){
	  	return code; 
	  }
	  public void setCode(String input){
	  	 this.code = input;
	  }
	  public String getStatus(){
	  	return status; 
	  }
	  public void setStatus(String input){
	  	 this.status = input;
	  }
	  public String getCommand(){
	  	return command; 
	  }
	  public void setCommand(String input){
	  	 this.command = input;
	  }
	  public String getMessage(){
	  	return message; 
	  }
	  public void setMessage(String input){
	  	 this.message = input;
	  }
	  public long getResponseTS(){
	  	return responseTS; 
	  }
	  public void setResponseTS(long input){
	  	 this.responseTS = input;
	  }
	  public String getVersion(){
	  	return version; 
	  }
	  public void setVersion(String input){
	  	 this.version = input;
	  }
	  public String getFormatId(){
	  	return formatId; 
	  }
	  public void setFormatId(String input){
	  	 this.formatId = input;
	  }
	  public String getFormatName(){
	  	return formatName; 
	  }
	  public void setFormatName(String input){
	  	 this.formatName = input;
	  }
	  public List<Tags> getTags(){
	  	return tags; 
	  }
	  public void setTags(List<Tags> input){
	  	 this.tags = input;
	  }
	public void setTags(String tagName, String macId, long locationTS, Double randomx, Double randomy, Double randomz) {
		Tags ltags = new Tags();
		ltags.setTagId(macId);
		ltags.setTagName(tagName);
		List<Double> location = new ArrayList<>();
		
		DecimalFormat df = new DecimalFormat("#.##");
		
		String test = df.format(randomx);
		Double testDouble = Double.parseDouble(test);
		location.add(testDouble);
		
		test = df.format(randomy);
		testDouble = Double.parseDouble(test);
		location.add(testDouble);
		
		test = df.format(randomz);
		testDouble = Double.parseDouble(test);
		location.add(testDouble);
		ltags.setLocationTS(locationTS);
		
		
		//System.out.println("Zone not found for Tagid : " + macId + "xpos: " + randomx + "ypos :" + randomy + "test : " + test + "testDouble : " + test);
		
		ltags.setLocation(location);
		tags.add(ltags);
		
		
	}
	public TagData() {
		super();
		
	}
}
  