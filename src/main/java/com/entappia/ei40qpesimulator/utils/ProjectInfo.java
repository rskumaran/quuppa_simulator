package com.entappia.ei40qpesimulator.utils;

import java.util.List;

public class ProjectInfo{
	public class Locators{
		private boolean locationLocked;
		private List<Double> orientation;
		private boolean visible; 
		private String notes; 
		private Double focusingErrorDeg; 
		private String color; 
		private boolean isOrientationSetManually;
		private List<String> associatedAreas;
		private boolean locationLockedX; 
		private String locatorChannel; 
		private boolean locationLockedZ; 
		private boolean locationLockedY; 
		private String locatorType; 
		private String name; 
		private List<Double> location;
		private String id; 


		public List<Double> getOrientation() {
			return orientation;
		}
		public void setOrientation(List<Double> orientation) {
			this.orientation = orientation;
		}
		public List<String> getAssociatedAreas() {
			return associatedAreas;
		}
		public void setAssociatedAreas(List<String> associatedAreas) {
			this.associatedAreas = associatedAreas;
		}
		public List<Double> getLocation() {
			return location;
		}
		public void setLocation(List<Double> location) {
			this.location = location;
		}
		public void setOrientationSetManually(boolean isOrientationSetManually) {
			this.isOrientationSetManually = isOrientationSetManually;
		}
		public boolean getLocationLocked(){
			return locationLocked; 
		}
		public void setLocationLocked(boolean input){
			this.locationLocked = input;
		}
		public boolean getVisible(){
			return visible; 
		}
		public void setVisible(boolean input){
			this.visible = input;
		}
		public String getNotes(){
			return notes; 
		}
		public void setNotes(String input){
			this.notes = input;
		}
		public Double getFocusingErrorDeg(){
			return focusingErrorDeg; 
		}
		public void setFocusingErrorDeg(Double input){
			this.focusingErrorDeg = input;
		}
		public String getColor(){
			return color; 
		}
		public void setColor(String input){
			this.color = input;
		}
		public boolean getIsOrientationSetManually(){
			return isOrientationSetManually; 
		}
		public void setIsOrientationSetManually(boolean input){
			this.isOrientationSetManually = input;
		}
		public boolean getLocationLockedX(){
			return locationLockedX; 
		}
		public void setLocationLockedX(boolean input){
			this.locationLockedX = input;
		}
		public String getLocatorChannel(){
			return locatorChannel; 
		}
		public void setLocatorChannel(String input){
			this.locatorChannel = input;
		}
		public boolean getLocationLockedZ(){
			return locationLockedZ; 
		}
		public void setLocationLockedZ(boolean input){
			this.locationLockedZ = input;
		}
		public boolean getLocationLockedY(){
			return locationLockedY; 
		}
		public void setLocationLockedY(boolean input){
			this.locationLockedY = input;
		}
		public String getLocatorType(){
			return locatorType; 
		}
		public void setLocatorType(String input){
			this.locatorType = input;
		}
		public String getName(){
			return name; 
		}
		public void setName(String input){
			this.name = input;
		}
		public String getId(){
			return id; 
		}
		public void setId(String input){
			this.id = input;
		}
	}
	public class BackgroundImages{
		private Double widthMeter; 
		private Double xMeter; 
		private boolean visible; 
		private String otherCoordSys; 
		private Double rotation; 
		private String base64; 
		private Double origoY; 
		private Double origoX; 
		private Double heightMeter; 
		private Double yMeter; 
		private Double alpha; 
		private String id; 
		private Double metersPerPixelY; 
		private Double metersPerPixelX; 

		public Double getWidthMeter(){
			return widthMeter; 
		}
		public void setWidthMeter(Double input){
			this.widthMeter = input;
		}
		public Double getXMeter(){
			return xMeter; 
		}
		public void setXMeter(Double input){
			this.xMeter = input;
		}
		public boolean getVisible(){
			return visible; 
		}
		public void setVisible(boolean input){
			this.visible = input;
		}
		public String getOtherCoordSys(){
			return otherCoordSys; 
		}
		public void setOtherCoordSys(String input){
			this.otherCoordSys = input;
		}
		public Double getRotation(){
			return rotation; 
		}
		public void setRotation(Double input){
			this.rotation = input;
		}
		public String getBase64(){
			return base64; 
		}
		public void setBase64(String input){
			this.base64 = input;
		}
		public Double getOrigoY(){
			return origoY; 
		}
		public void setOrigoY(Double input){
			this.origoY = input;
		}
		public Double getOrigoX(){
			return origoX; 
		}
		public void setOrigoX(Double input){
			this.origoX = input;
		}
		public Double getHeightMeter(){
			return heightMeter; 
		}
		public void setHeightMeter(Double input){
			this.heightMeter = input;
		}
		public Double getYMeter(){
			return yMeter; 
		}
		public void setYMeter(Double input){
			this.yMeter = input;
		}
		public Double getAlpha(){
			return alpha; 
		}
		public void setAlpha(Double input){
			this.alpha = input;
		}
		public String getId(){
			return id; 
		}
		public void setId(String input){
			this.id = input;
		}
		public Double getMetersPerPixelY(){
			return metersPerPixelY; 
		}
		public void setMetersPerPixelY(Double input){
			this.metersPerPixelY = input;
		}
		public Double getMetersPerPixelX(){
			return metersPerPixelX; 
		}
		public void setMetersPerPixelX(Double input){
			this.metersPerPixelX = input;
		}
	}
	public class TrackingAreas{
		private String notes; 
		private String name; 
		private String id; 
		private Double minZ; 
		private boolean track3d; 

		public String getNotes(){
			return notes; 
		}
		public void setNotes(String input){
			this.notes = input;
		}
		public String getName(){
			return name; 
		}
		public void setName(String input){
			this.name = input;
		}
		public String getId(){
			return id; 
		}
		public void setId(String input){
			this.id = input;
		}
		public Double getMinZ(){
			return minZ; 
		}
		public void setMinZ(Double input){
			this.minZ = input;
		}
		public boolean getTrack3d(){
			return track3d; 
		}
		public void setTrack3d(boolean input){
			this.track3d = input;
		}
	}
	public class Polygons{
		private boolean locationLocked; 
		private String notes; 
		private boolean visible; 
		private String color; 
		private List<TrackingAreas> trackingAreas; 
		private String name; 
		private String polygonData; 
		private String id;
		private List<String> polygonHoles;


		public List<String> getPolygonHoles() {
			return polygonHoles;
		}
		public void setPolygonHoles(List<String> polygonHoles) {
			this.polygonHoles = polygonHoles;
		}
		public boolean getLocationLocked(){
			return locationLocked; 
		}
		public void setLocationLocked(boolean input){
			this.locationLocked = input;
		}
		public String getNotes(){
			return notes; 
		}
		public void setNotes(String input){
			this.notes = input;
		}
		public boolean getVisible(){
			return visible; 
		}
		public void setVisible(boolean input){
			this.visible = input;
		}
		public String getColor(){
			return color; 
		}
		public void setColor(String input){
			this.color = input;
		}
		public List<TrackingAreas> getTrackingAreas(){
			return trackingAreas; 
		}
		public void setTrackingAreas(List<TrackingAreas> input){
			this.trackingAreas = input;
		}
		public String getName(){
			return name; 
		}
		public void setName(String input){
			this.name = input;
		}
		public String getPolygonData(){
			return polygonData; 
		}
		public void setPolygonData(String input){
			this.polygonData = input;
		}
		public String getId(){
			return id; 
		}
		public void setId(String input){
			this.id = input;
		}
	}
	public class Zones{
		private boolean locationLocked; 
		private String notes; 
		private boolean visible; 
		private String color; 
		private String zoneGroupId; 
		private String name; 
		private String polygonData; 
		private String id;
		private List<String> polygonHoles;
		private List<Double> openings;


		public List<String> getPolygonHoles() {
			return polygonHoles;
		}
		public void setPolygonHoles(List<String> polygonHoles) {
			this.polygonHoles = polygonHoles;
		}
		public List<Double> getOpenings() {
			return openings;
		}
		public void setOpenings(List<Double> openings) {
			this.openings = openings;
		}
		public boolean getLocationLocked(){
			return locationLocked; 
		}
		public void setLocationLocked(boolean input){
			this.locationLocked = input;
		}
		public String getNotes(){
			return notes; 
		}
		public void setNotes(String input){
			this.notes = input;
		}
		public boolean getVisible(){
			return visible; 
		}
		public void setVisible(boolean input){
			this.visible = input;
		}
		public String getColor(){
			return color; 
		}
		public void setColor(String input){
			this.color = input;
		}
		public String getZoneGroupId(){
			return zoneGroupId; 
		}
		public void setZoneGroupId(String input){
			this.zoneGroupId = input;
		}
		public String getName(){
			return name; 
		}
		public void setName(String input){
			this.name = input;
		}
		public String getPolygonData(){
			return polygonData; 
		}
		public void setPolygonData(String input){
			this.polygonData = input;
		}
		public String getId(){
			return id; 
		}
		public void setId(String input){
			this.id = input;
		}
	}
	public class CoordinateSystems{
		private List<Locators> locators; 
		private List<BackgroundImages> backgroundImages; 
		private Double relativeZ; 
		private List<Polygons> polygons; 
		private String name; 
		private String id; 
		private List<Zones> zones; 

		public List<Locators> getLocators(){
			return locators; 
		}
		public void setLocators(List<Locators> input){
			this.locators = input;
		}
		public List<BackgroundImages> getBackgroundImages(){
			return backgroundImages; 
		}
		public void setBackgroundImages(List<BackgroundImages> input){
			this.backgroundImages = input;
		}
		public Double getRelativeZ(){
			return relativeZ; 
		}
		public void setRelativeZ(Double input){
			this.relativeZ = input;
		}
		public List<Polygons> getPolygons(){
			return polygons; 
		}
		public void setPolygons(List<Polygons> input){
			this.polygons = input;
		}
		public String getName(){
			return name; 
		}
		public void setName(String input){
			this.name = input;
		}
		public String getId(){
			return id; 
		}
		public void setId(String input){
			this.id = input;
		}
		public List<Zones> getZones(){
			return zones; 
		}
		public void setZones(List<Zones> input){
			this.zones = input;
		}
		public double getminx() {

			return backgroundImages.get(0).getXMeter();
		}
		public double getmaxx() {

			return backgroundImages.get(0).getWidthMeter();
		}
		public double getminy() {

			return backgroundImages.get(0).getYMeter();
		}
		public double getmaxy() {

			return backgroundImages.get(0).getHeightMeter();
		}
		public double getOriginX() {
			return backgroundImages.get(0).getOrigoX();
		}
		public double getMetersPerPixelX() {
			return backgroundImages.get(0).getMetersPerPixelX();
		}
		public double getOriginY() {
			return backgroundImages.get(0).getOrigoY();
		}
		public double getMetersPerPixelY() {
			return backgroundImages.get(0).getMetersPerPixelY();
		}
	}
	public class TagGroups{
		private Double trackHeight; 
		private String notes; 
		private String color; 
		private boolean sensorOnly; 
		private String name; 
		private boolean unknownTags; 
		private String id; 
		private boolean track3d; 
		private boolean track; 

		public Double getTrackHeight(){
			return trackHeight; 
		}
		public void setTrackHeight(Double input){
			this.trackHeight = input;
		}
		public String getNotes(){
			return notes; 
		}
		public void setNotes(String input){
			this.notes = input;
		}
		public String getColor(){
			return color; 
		}
		public void setColor(String input){
			this.color = input;
		}
		public boolean getSensorOnly(){
			return sensorOnly; 
		}
		public void setSensorOnly(boolean input){
			this.sensorOnly = input;
		}
		public String getName(){
			return name; 
		}
		public void setName(String input){
			this.name = input;
		}
		public boolean getUnknownTags(){
			return unknownTags; 
		}
		public void setUnknownTags(boolean input){
			this.unknownTags = input;
		}
		public String getId(){
			return id; 
		}
		public void setId(String input){
			this.id = input;
		}
		public boolean getTrack3d(){
			return track3d; 
		}
		public void setTrack3d(boolean input){
			this.track3d = input;
		}
		public boolean getTrack(){
			return track; 
		}
		public void setTrack(boolean input){
			this.track = input;
		}
	}
	public class ZoneGroups{
		private String notes; 
		private String color; 
		private boolean smartZone; 
		private boolean hideLocation; 
		private List<String> activationCommands; 
		private String name; 
		private String id; 
		private boolean rfBlocking; 

		public String getNotes(){
			return notes; 
		}
		public void setNotes(String input){
			this.notes = input;
		}
		public String getColor(){
			return color; 
		}
		public void setColor(String input){
			this.color = input;
		}
		public boolean getSmartZone(){
			return smartZone; 
		}
		public void setSmartZone(boolean input){
			this.smartZone = input;
		}
		public boolean getHideLocation(){
			return hideLocation; 
		}
		public void setHideLocation(boolean input){
			this.hideLocation = input;
		}
		public List<String> getActivationCommands(){
			return activationCommands; 
		}
		public void setActivationCommands(List<String> input){
			this.activationCommands = input;
		}
		public String getName(){
			return name; 
		}
		public void setName(String input){
			this.name = input;
		}
		public String getId(){
			return id; 
		}
		public void setId(String input){
			this.id = input;
		}
		public boolean getRfBlocking(){
			return rfBlocking; 
		}
		public void setRfBlocking(boolean input){
			this.rfBlocking = input;
		}
	}

	private int code; 
	private String command; 
	private List<CoordinateSystems> coordinateSystems; 
	private String message; 
	private long responseTS; 
	private String status; 
	private List<TagGroups> tagGroups; 
	private String version; 
	private List<ZoneGroups> zoneGroups; 

	public int getCode(){
		return code; 
	}
	public void setCode(int input){
		this.code = input;
	}
	public String getCommand(){
		return command; 
	}
	public void setCommand(String input){
		this.command = input;
	}
	public List<CoordinateSystems> getCoordinateSystems(){
		return coordinateSystems; 
	}
	public void setCoordinateSystems(List<CoordinateSystems> input){
		this.coordinateSystems = input;
	}
	public String getMessage(){
		return message; 
	}
	public void setMessage(String input){
		this.message = input;
	}
	public long getResponseTS(){
		return responseTS; 
	}
	public void setResponseTS(long input){
		this.responseTS = input;
	}
	public String getStatus(){
		return status; 
	}
	public void setStatus(String input){
		this.status = input;
	}
	public List<TagGroups> getTagGroups(){
		return tagGroups; 
	}
	public void setTagGroups(List<TagGroups> input){
		this.tagGroups = input;
	}
	public String getVersion(){
		return version; 
	}
	public void setVersion(String input){
		this.version = input;
	}
	public List<ZoneGroups> getZoneGroups(){
		return zoneGroups; 
	}
	public void setZoneGroups(List<ZoneGroups> input){
		this.zoneGroups = input;
	}
	public double getminx() {

		return coordinateSystems.get(0).getminx();
	}
	public double getmaxx() {

		return coordinateSystems.get(0).getmaxx();
	}
	public double getminy() {

		return coordinateSystems.get(0).getminy();
	}
	public double getmaxy() {

		return coordinateSystems.get(0).getmaxy();
	}
	public double getOriginX() {
		
		return coordinateSystems.get(0).getOriginX();
	}
	public double getMetersPerPixelX() {
		
		return coordinateSystems.get(0).getMetersPerPixelX();
	}
	public double getOriginY() {
		
		return coordinateSystems.get(0).getOriginY();
	}
	public double getMetersPerPixelY() {
		
		return coordinateSystems.get(0).getMetersPerPixelY();
	}
}
